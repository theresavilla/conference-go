# from common.modelEncoder import ModelEncoder
# from .models import Conference, Location


# class LocationListEncoder(ModelEncoder):
#     model = Location
#     properties = ["name"]


# class LocationDetailEncoder(ModelEncoder):
#     model = Location
#     properties = ["name", "city", "state", "room_count", "created", "updated"]

#     # to convert a more complex data type (like a foreign key)
#     # to a simple, serializable data
#     def get_extra_data(self, o):
#         return {"state": o.state.abbreviation}


# class ConferenceListEncoder(ModelEncoder):
#     model = Conference
#     # modelencoder has a built in get_api_url so you don't need to specify it
#     properties = ["name"]


# class ConferenceDetailEncoder(ModelEncoder):
#     model = Conference
#     # define what property of conference you want to convert to JSON
#     properties = ["name", "starts", "ends", "location", "description"]
#     # I think this does the same thing as def get_extra_data above
#     encoders = {"location": LocationListEncoder}
