# this is the place where you do your local anti-corruption layers

from .keys import PEXEL_API_KEY, OPEN_WEATHER_API_KEY
import requests


def get_image(city, state):
    headers = {"Authorization": PEXEL_API_KEY}

    res = requests.get(
        f"https://api.pexels.com/v1/search?query={city}+{state}&per_page=1",
        headers=headers,
    )

    return res.json()["photos"][0]["src"]["original"]


def get_coordinates(city, state):
    coordinates_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},USA&limit=1&appid={OPEN_WEATHER_API_KEY}"
    res = requests.get(coordinates_url)
    try:
        return {"lat": res.json()[0]["lat"], "lon": res.json()[0]["lon"]}
    except IndexError:
        return {"weather": "null"}


def get_weather(lat, lon):
    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial"

    res = requests.get(weather_url)
    # within weather you are taking description and temperature
    response = {
        "description": res.json()["weather"][0]["description"],
        "current_temp": res.json()["main"]["temp"],
    }
    return response
