from django.http import JsonResponse
from django.views.decorators.http import require_http_methods

# from .encoders import (
#     ConferenceDetailEncoder,
#     ConferenceListEncoder,
#     LocationDetailEncoder,
#     LocationListEncoder,
# )
from .models import Conference, Location, State
from common.json import ModelEncoder
import json
from .acls import get_image, get_coordinates, get_weather


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "picture_url",
    ]

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    """
    Lists the conference names and the link to the conference.

    Returns a dictionary with a single key "conferences" which
    is a list of conference names and URLS. Each entry in the list
    is a dictionary that contains the name of the conference and
    the link to the conference's information.

    {
        "conferences": [
            {
                "name": conference's name,
                "href": URL to the conference,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        conferences = Conference.objects.all()
        # pass in data, make an encoder that's going to handle your conversion
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
        )
    else:
        content = json.loads(request.body)

        # get the Location object and put it in the content dict
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse({"message": "Invalid location id"}, status=400)

        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference, encoder=ConferenceDetailEncoder, safe=False
        )
    # return JsonResponse(conferences, encoder=ConferenceListEncoder, safe=False)

    # longer and manual version instead of using Encoders
    # response = []
    # conferences = Conference.objects.all()
    # for conference in conferences:
    #     response.append(
    #         {
    #             "name": conference.name,
    #             "href": conference.get_api_url(),
    #         }
    #     )
    # return JsonResponse({"conferences": response})


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_conference(request, pk):
    """
    Returns the details for the Conference model specified
    by the pk parameter.

    This should return a dictionary with the name, starts,
    ends, description, created, updated, max_presentations,
    max_attendees, and a dictionary for the location containing
    its name and href.

    {
        "name": the conference's name,
        "starts": the date/time when the conference starts,
        "ends": the date/time when the conference ends,
        "description": the description of the conference,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "max_presentations": the maximum number of presentations,
        "max_attendees": the maximum number of attendees,
        "location": {
            "name": the name of the location,
            "href": the URL for the location,
        }
    }
    """
    if request.method == "GET":
        conference = Conference.objects.get(id=pk)
        coordinates = get_coordinates(
            conference.location.city,
            conference.location.state.abbreviation,
        )
        try:
            weather = get_weather(coordinates["lat"], coordinates["lon"])
            return JsonResponse(
                {"conference": conference, "weather": weather},
                encoder=ConferenceDetailEncoder,
                safe=False,
            )
        except KeyError:
            return JsonResponse(
                {"conference": conference, "weather": "null"},
                encoder=ConferenceDetailEncoder,
                safe=False,
            )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        # convert the submitted JSON-formatted string into a dictionary
        content = json.loads(request.body)
        try:
            # convert the state abbreviation into a State, it it exists
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"}, status=400
            )

        # use that dictionary to update the existing Location
        Conference.objects.filter(id=pk).update(**content)

        conference = Conference.objects.get(id=pk)
        # returns updated location
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )

    # longer and manual version instead of using Encoders
    # conference = Conference.objects.get(id=pk)
    # return JsonResponse(
    #     {
    #         "name": conference.name,
    #         "starts": conference.starts,
    #         "ends": conference.ends,
    #         "description": conference.description,
    #         "created": conference.created,
    #         "updated": conference.updated,
    #         "max_presentations": conference.max_presentations,
    #         "max_attendees": conference.max_attendees,
    #         "location": {
    #             "name": conference.location.name,
    #             "href": conference.location.get_api_url(),
    #         },
    #     }
    # )


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    """
    Lists the location names and the link to the location.

    Returns a dictionary with a single key "locations" which
    is a list of location names and URLS. Each entry in the list
    is a dictionary that contains the name of the location and
    the link to the location's information.

    {
        "locations": [
            {
                "name": location's name,
                "href": URL to the location,
            },
            ...
        ]
    }
    """
    # the next 6 lines will only run if the HTTP method is GET
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationListEncoder,
            safe=False,
        )
    # this else handles the POST
    else:
        # the content of the POST is a JSON-formatted string stored
        # in request.body, so we need to decode that using
        # json.loads
        content = json.loads(request.body)

        # get the city and state from content
        # make a request to pexel for an image
        # get first image from pexel request
        # add that image to my new model
        try:
            content["picture_url"] = get_image(
                content["city"], content["state"]
            )
        except IndexError:
            content["picture_url"] = "null"

        try:
            # get the State object and put it in the content dict
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state

            location = Location.objects.create(**content)
            return JsonResponse(
                location, encoder=LocationDetailEncoder, safe=False
            )
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"}, status=400
            )

    # longer and manual version instead of using Encoders
    # locations = [
    #     {
    #         "name": location.name,
    #         "href": location.get_api_url(),
    #     }
    #     for location in Location.objects.all()
    # ]
    # return JsonResponse({"locations": locations})


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_location(request, pk):
    """
    Returns the details for the Location model specified
    by the pk parameter.

    This should return a dictionary with the name, city,
    room count, created, updated, and state abbreviation.

    {
        "name": location's name,
        "city": location's city,
        "room_count": the number of rooms available,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "state": the two-letter abbreviation for the state,
    }
    """
    # setting the next 4 lines to only run if the HTTP method is GET
    if request.method == "GET":
        location = Location.objects.get(id=pk)
        return JsonResponse(
            location, encoder=LocationDetailEncoder, safe=False
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        # convert the submitted JSON-formatted string into a dictionary
        content = json.loads(request.body)
        try:
            # convert the state abbreviation into a State, it it exists
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"}, status=400
            )

        # use that dictinoary to update the existing Location
        Location.objects.filter(id=pk).update(**content)

        location = Location.objects.get(id=pk)
        # returns updated location
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )

    # longer and manual version instead of using Encoders
    # location = Location.objects.get(id=pk)
    # return JsonResponse(
    #     {
    #         "name": location.name,
    #         "city": location.city,
    #         "room_count": location.room_count,
    #         "created": location.created,
    #         "updated": location.updated,
    #         "state": location.state.abbreviation,
    #     }
    # )
