from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet


class DateEncoder(JSONEncoder):
    def default(self, o):
        # if o is an instance of datetime
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        # if o is an instance of a QuerySet, turn it into a normal list
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


"""Idea behind ModelEncoder is...
#   if the object to decode is the same class as what's in the
        #   model property, then
        #     * create an empty dictionary that will hold the property names
        #       as keys and the property values as values
        #     * for each name in the properties list
        #         * get the value of that property from the model instance
        #           given just the property name
        #         * put it into the dictionary with that property name as
        #           the key
        #     * return the dictionary
        #   otherwise,
        #       return super().default(o)  # From the documentation"""


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        # check if the incoming object stored in the o parameter
        # is an instance of the class stored in the self.model property
        if isinstance(o, self.model):
            # create an empty dictionary that will hold the propery names
            # as keys and the propery values as values
            d = {}
            # if o has the attribute get_api_url
            if hasattr(o, "get_api_url"):
                # add its return value to the dictionary with the key "href"
                d["href"] = o.get_api_url()
            # for each name in the properties list (properties came from
            # properties on the encoder)
            for property in self.properties:
                # get the value of the property from the model instance
                # given just the property name
                value = getattr(o, property)
                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)
                # put the value in the dictionarywith that property name
                # as the key
                d[property] = value
            d.update(self.get_extra_data(o))
            # return the dictionary
            return d
        # this else statement comes from documentation
        else:
            return super().default(o)

    def get_extra_data(self, o):
        return {}
