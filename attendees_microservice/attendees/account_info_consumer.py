from datetime import datetime
import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()

from attendees.models import AccountVO


# Declare a function to update the AccountVO object
def update_AccountVO(ch, method, properties, body):
    # content = load the json in body
    content = json.loads(body)
    print(content)
    first_name = content["first_name"]
    last_name = content["last_name"]
    email = content["email"]
    is_active = content["is_active"]
    updated_string = content["updated"]
    updated = datetime.fromisoformat(updated_string)
    if is_active:
        # use the update_or_create method of the AccountVO.objects QuerySet
        # to update or create the AccountVO object
        # only takes 1-2 parameters - email is a unique key
        # default=dictionary of fields that make up model
        AccountVO.objects.filter(updated=updated).update_or_create(
            email=email,
            defaults={
                "first_name": first_name,
                "last_name": last_name,
                "updated": updated,
                "is_active": is_active,
            },
        )

    else:
        # delete the AccountVO object with the specified email, if it exists
        AccountVO.objects.filter(email=email).delete()


while True:
    try:
        # create the pika connection parameters
        parameters = pika.ConnectionParameters(host="rabbitmq")

        # create a blocking connection with the parameters
        connection = pika.BlockingConnection(parameters)

        # open a channel
        channel = connection.channel()

        # declare a fanout exchange named "account_info"
        channel.exchange_declare(
            exchange="account_info", exchange_type="fanout"
        )

        # declare a randomly named queue
        result = channel.queue_declare(queue="", exclusive=True)

        # get the queue name fo the randomly-named queue
        queue_name = result.method.queue

        # bind the queue to the "account_info" exchange
        channel.queue_bind(exchange="account_info", queue=queue_name)

        # do a basic_consume for the queue name that calls function above
        channel.basic_consume(
            queue=queue_name,
            on_message_callback=update_AccountVO,
            auto_ack=True,
        )
        channel.start_consuming()

    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
